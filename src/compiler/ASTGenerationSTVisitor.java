package compiler;

import static compiler.lib.FOOLlib.extractCtxName;
import static compiler.lib.FOOLlib.lowerizeFirstChar;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;

import compiler.AST.AndNode;
import compiler.AST.ArrowTypeNode;
import compiler.AST.BoolNode;
import compiler.AST.BoolTypeNode;
import compiler.AST.CallNode;
import compiler.AST.ClassCallNode;
import compiler.AST.ClassNode;
import compiler.AST.DivNode;
import compiler.AST.EmptyNode;
import compiler.AST.EqualNode;
import compiler.AST.FieldNode;
import compiler.AST.FunNode;
import compiler.AST.GreaterEqualNode;
import compiler.AST.IdNode;
import compiler.AST.IfNode;
import compiler.AST.IntNode;
import compiler.AST.IntTypeNode;
import compiler.AST.LessEqualNode;
import compiler.AST.MethodNode;
import compiler.AST.MinusNode;
import compiler.AST.NewNode;
import compiler.AST.NotNode;
import compiler.AST.OrNode;
import compiler.AST.ParNode;
import compiler.AST.PlusNode;
import compiler.AST.PrintNode;
import compiler.AST.ProgLetInNode;
import compiler.AST.ProgNode;
import compiler.AST.RefTypeNode;
import compiler.AST.TimesNode;
import compiler.AST.VarNode;
import compiler.FOOLParser.AndOrContext;
import compiler.FOOLParser.ArrowContext;
import compiler.FOOLParser.BoolTypeContext;
import compiler.FOOLParser.CallContext;
import compiler.FOOLParser.CldecContext;
import compiler.FOOLParser.CompContext;
import compiler.FOOLParser.DecContext;
import compiler.FOOLParser.DotCallContext;
import compiler.FOOLParser.ExpContext;
import compiler.FOOLParser.FalseContext;
import compiler.FOOLParser.FundecContext;
import compiler.FOOLParser.HotypeContext;
import compiler.FOOLParser.IdContext;
import compiler.FOOLParser.IdTypeContext;
import compiler.FOOLParser.IfContext;
import compiler.FOOLParser.IntTypeContext;
import compiler.FOOLParser.IntegerContext;
import compiler.FOOLParser.LetInProgContext;
import compiler.FOOLParser.MethdecContext;
import compiler.FOOLParser.NewContext;
import compiler.FOOLParser.NoDecProgContext;
import compiler.FOOLParser.NotContext;
import compiler.FOOLParser.NullContext;
import compiler.FOOLParser.ParsContext;
import compiler.FOOLParser.PlusMinusContext;
import compiler.FOOLParser.PrintContext;
import compiler.FOOLParser.ProgContext;
import compiler.FOOLParser.TimesDivContext;
import compiler.FOOLParser.TrueContext;
import compiler.FOOLParser.VardecContext;
import compiler.lib.DecNode;
import compiler.lib.Node;
import compiler.lib.TypeNode;

public class ASTGenerationSTVisitor extends FOOLBaseVisitor<Node> {

	String indent;
	public boolean print;

	ASTGenerationSTVisitor() {
	}

	ASTGenerationSTVisitor(boolean debug) {
		print = debug;
	}

	private void printVarAndProdName(ParserRuleContext ctx) {
		String prefix = "";
		Class<?> ctxClass = ctx.getClass(), parentClass = ctxClass.getSuperclass();
		if (!parentClass.equals(ParserRuleContext.class)) // parentClass is the var context (and not ctxClass itself)
			prefix = lowerizeFirstChar(extractCtxName(parentClass.getName())) + ": production #";
		System.out.println(indent + prefix + lowerizeFirstChar(extractCtxName(ctxClass.getName())));
	}

	@Override
	public Node visit(ParseTree t) {
		if (t == null)
			return null;
		String temp = indent;
		indent = (indent == null) ? "" : indent + "  ";
		Node result = super.visit(t);
		indent = temp;
		return result;
	}

	@Override
	public Node visitProg(ProgContext c) {
		if (print)
			printVarAndProdName(c);
		return visit(c.progbody());
	}

	@Override
	public Node visitLetInProg(final LetInProgContext c) {
		if (print)
			printVarAndProdName(c);
		List<DecNode> declist = new ArrayList<>();
		for (CldecContext dec : c.cldec())
			declist.add((DecNode) visit(dec));
		for (DecContext dec : c.dec())
			declist.add((DecNode) visit(dec));
		return new ProgLetInNode(declist, visit(c.exp()));
	}

	@Override
	public Node visitNoDecProg(NoDecProgContext c) {
		if (print)
			printVarAndProdName(c);
		return new ProgNode(visit(c.exp()));
	}

	@Override
	public Node visitTimesDiv(final TimesDivContext ctx) {
		if (print) {
			printVarAndProdName(ctx);
		}
		if (ctx.TIMES() != null) {
			final Node node = new TimesNode(visit(ctx.exp(0)), visit(ctx.exp(1)));
			node.setLine(ctx.TIMES().getSymbol().getLine());
			return node;
		}
		final Node node = new DivNode(visit(ctx.exp(0)), visit(ctx.exp(1)));
		node.setLine(ctx.DIV().getSymbol().getLine());
		return node;
	}

	@Override
	public Node visitPlusMinus(final PlusMinusContext ctx) {
		if (print) {
			printVarAndProdName(ctx);
		}
		if (ctx.PLUS() != null) {
			final Node node = new PlusNode(visit(ctx.exp(0)), visit(ctx.exp(1)));
			node.setLine(ctx.PLUS().getSymbol().getLine());
			return node;
		}
		final Node node = new MinusNode(visit(ctx.exp(0)), visit(ctx.exp(1)));
		node.setLine(ctx.MINUS().getSymbol().getLine());
		return node;
	}

	@Override
	public Node visitComp(final CompContext ctx) {
		if (print) {
			printVarAndProdName(ctx);
		}
		if (ctx.EQ() != null) {
			final Node node = new EqualNode(visit(ctx.exp(0)), visit(ctx.exp(1)));
			node.setLine(ctx.EQ().getSymbol().getLine());
			return node;
		} else if (ctx.GE() != null) {
			final var node = new GreaterEqualNode(visit(ctx.exp(0)), visit(ctx.exp(1)));
			node.setLine(ctx.GE().getSymbol().getLine());
			return node;
		}
		final Node node = new LessEqualNode(visit(ctx.exp(0)), visit(ctx.exp(1)));
		node.setLine(ctx.LE().getSymbol().getLine());
		return node;
	}

	@Override
	public Node visitAndOr(final AndOrContext ctx) {
		if (print) {
			printVarAndProdName(ctx);
		}
		if (ctx.AND() != null) {
			final Node node = new AndNode(visit(ctx.exp(0)), visit(ctx.exp(1)));
			node.setLine(ctx.AND().getSymbol().getLine());
			return node;
		}
		final Node node = new OrNode(visit(ctx.exp(0)), visit(ctx.exp(1)));
		node.setLine(ctx.OR().getSymbol().getLine());
		return node;
	}

	@Override
	public Node visitNot(final NotContext ctx) {
		if (print) {
			printVarAndProdName(ctx);
		}
		final Node node = new NotNode(visit(ctx.exp()));
		node.setLine(ctx.NOT().getSymbol().getLine());
		return node;
	}

	@Override
	public Node visitVardec(VardecContext c) {
		if (print)
			printVarAndProdName(c);
		Node n = null;
		if (c.ID() != null) { // non-incomplete ST
			n = new VarNode(c.ID().getText(), (TypeNode) visit(c.hotype()), visit(c.exp()));
			n.setLine(c.VAR().getSymbol().getLine());
		}
		return n;
	}

	@Override
	public Node visitFundec(FundecContext c) {
		if (print)
			printVarAndProdName(c);
		List<ParNode> parList = new ArrayList<>();
		for (int i = 1; i < c.ID().size(); i++) {
			ParNode p = new ParNode(c.ID(i).getText(), (TypeNode) visit(c.hotype(i - 1)));
			p.setLine(c.ID(i).getSymbol().getLine());
			parList.add(p);
		}
		List<DecNode> decList = new ArrayList<>();
		for (DecContext dec : c.dec())
			decList.add((DecNode) visit(dec));
		Node n = null;
		if (c.ID().size() > 0) { // non-incomplete ST
			n = new FunNode(c.ID(0).getText(), (TypeNode) visit(c.type()), parList, decList, visit(c.exp()));
			n.setLine(c.FUN().getSymbol().getLine());
		}
		return n;
	}

	@Override
	public Node visitIntType(IntTypeContext c) {
		if (print)
			printVarAndProdName(c);
		return new IntTypeNode();
	}

	@Override
	public Node visitBoolType(BoolTypeContext c) {
		if (print)
			printVarAndProdName(c);
		return new BoolTypeNode();
	}

	@Override
	public Node visitIdType(final IdTypeContext ctx) {
		if (print) {
			printVarAndProdName(ctx);
		}
		return new RefTypeNode(ctx.ID().getText());
	}

	@Override
	public Node visitArrow(ArrowContext c) {
		List<TypeNode> parList = new ArrayList<>();
		for (final HotypeContext ctx : c.hotype()) {
			parList.add((TypeNode) visit(ctx));
		}
		return new ArrowTypeNode(parList, (TypeNode) visit(c.type()));
	}

	@Override
	public Node visitInteger(IntegerContext c) {
		if (print)
			printVarAndProdName(c);
		int v = Integer.parseInt(c.NUM().getText());
		return new IntNode(c.MINUS() == null ? v : -v);
	}

	@Override
	public Node visitTrue(TrueContext c) {
		if (print)
			printVarAndProdName(c);
		return new BoolNode(true);
	}

	@Override
	public Node visitFalse(FalseContext c) {
		if (print)
			printVarAndProdName(c);
		return new BoolNode(false);
	}

	@Override
	public Node visitIf(IfContext c) {
		if (print)
			printVarAndProdName(c);
		Node ifNode = visit(c.exp(0));
		Node thenNode = visit(c.exp(1));
		Node elseNode = visit(c.exp(2));
		Node n = new IfNode(ifNode, thenNode, elseNode);
		n.setLine(c.IF().getSymbol().getLine());
		return n;
	}

	@Override
	public Node visitPrint(PrintContext c) {
		if (print)
			printVarAndProdName(c);
		return new PrintNode(visit(c.exp()));
	}

	@Override
	public Node visitPars(ParsContext c) {
		if (print)
			printVarAndProdName(c);
		return visit(c.exp());
	}

	@Override
	public Node visitId(IdContext c) {
		if (print)
			printVarAndProdName(c);
		Node n = new IdNode(c.ID().getText());
		n.setLine(c.ID().getSymbol().getLine());
		return n;
	}

	@Override
	public Node visitCall(CallContext c) {
		if (print)
			printVarAndProdName(c);
		List<Node> arglist = new ArrayList<>();
		for (ExpContext arg : c.exp())
			arglist.add(visit(arg));
		Node n = new CallNode(c.ID().getText(), arglist);
		n.setLine(c.ID().getSymbol().getLine());
		return n;
	}

	@Override
	public Node visitCldec(final CldecContext ctx) {
		if (print) {
			printVarAndProdName(ctx);
		}
		final boolean superClass = ctx.EXTENDS() != null;
		final int shift = superClass ? 2 : 1;
		if ((!superClass && ctx.ID().size() > 0) || (superClass && ctx.ID().size() > 1)) {
			final String className = ctx.ID(0).getText();
			final Node node = new ClassNode(className, superClass ? ctx.ID(1).getText() : null,
					IntStream.range(shift, ctx.ID().size()).mapToObj(i -> {
						final FieldNode field = new FieldNode(ctx.ID(i).getText(),
								(TypeNode) visit(ctx.type(i - shift)));
						field.setLine(ctx.ID(i).getSymbol().getLine());
						return field;
					}).collect(Collectors.toList()),
					ctx.methdec().stream().map(c -> (MethodNode) visit(c)).collect(Collectors.toList()));
			node.setLine(ctx.ID(0).getSymbol().getLine());
			return node;
		}
		return null;
	}

	@Override
	public Node visitMethdec(final MethdecContext ctx) {
		if (print) {
			printVarAndProdName(ctx);
		}
		if (ctx.ID().size() > 0) {
			final Node node = new MethodNode(ctx.ID(0).getText(), (TypeNode) visit(ctx.type()),
					IntStream.range(1, ctx.ID().size()).mapToObj(i -> {
						final var parNode = new ParNode(ctx.ID(i).getText(), (TypeNode) visit(ctx.hotype(i - 1)));
						parNode.setLine(ctx.ID(i).getSymbol().getLine());
						return parNode;
					}).collect(Collectors.toList()),
					ctx.dec().stream().map(d -> (DecNode) visit(d)).collect(Collectors.toList()), visit(ctx.exp()));
			node.setLine(ctx.FUN().getSymbol().getLine());
			return node;
		}
		return null;
	}

	@Override
	public Node visitNull(final NullContext ctx) {
		if (print) {
			printVarAndProdName(ctx);
		}
		return new EmptyNode();
	}

	@Override
	public Node visitNew(final NewContext ctx) {
		if (print) {
			printVarAndProdName(ctx);
		}
		if (ctx.ID() != null) {
			final var classTerminalNode = ctx.ID();
			final var node = new NewNode(classTerminalNode.getText(),
					ctx.exp().stream().map(this::visit).collect(Collectors.toList()));
			node.setLine(classTerminalNode.getSymbol().getLine());
			return node;
		}
		return null;
	}

	@Override
	public Node visitDotCall(final DotCallContext ctx) {
		if (print) {
			printVarAndProdName(ctx);
		}
		if (ctx.ID().size() > 1) {
			final var variableTerminal = ctx.ID(0);
			final var node = new ClassCallNode(variableTerminal.getText(), ctx.ID(1).getText(),
					ctx.exp().stream().map(this::visit).collect(Collectors.toList()));
			node.setLine(variableTerminal.getSymbol().getLine());
			return node;
		}
		return null;
	}
}
