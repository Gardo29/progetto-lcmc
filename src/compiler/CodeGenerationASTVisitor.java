package compiler;

import static compiler.lib.FOOLlib.freshFunLabel;
import static compiler.lib.FOOLlib.freshLabel;
import static compiler.lib.FOOLlib.getCode;
import static compiler.lib.FOOLlib.nlJoin;
import static compiler.lib.FOOLlib.putCode;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import compiler.AST.AndNode;
import compiler.AST.ArrowTypeNode;
import compiler.AST.BoolNode;
import compiler.AST.CallNode;
import compiler.AST.ClassCallNode;
import compiler.AST.ClassNode;
import compiler.AST.DivNode;
import compiler.AST.EmptyNode;
import compiler.AST.EqualNode;
import compiler.AST.FunNode;
import compiler.AST.GreaterEqualNode;
import compiler.AST.IdNode;
import compiler.AST.IfNode;
import compiler.AST.IntNode;
import compiler.AST.LessEqualNode;
import compiler.AST.MethodNode;
import compiler.AST.MethodTypeNode;
import compiler.AST.MinusNode;
import compiler.AST.NewNode;
import compiler.AST.NotNode;
import compiler.AST.OrNode;
import compiler.AST.ParNode;
import compiler.AST.PlusNode;
import compiler.AST.PrintNode;
import compiler.AST.ProgLetInNode;
import compiler.AST.ProgNode;
import compiler.AST.TimesNode;
import compiler.AST.VarNode;
import compiler.exc.VoidException;
import compiler.lib.BaseASTVisitor;
import compiler.lib.DecNode;
import compiler.lib.Node;
import svm.ExecuteVM;

public class CodeGenerationASTVisitor extends BaseASTVisitor<String, VoidException> {

	private final List<List<String>> dispatchTables = new ArrayList<>();

	CodeGenerationASTVisitor() {
	}

	CodeGenerationASTVisitor(boolean debug) {
		super(false, debug);
	} // enables print for debugging

	@Override
	public String visitNode(ProgLetInNode n) {
		if (print)
			printNode(n);
		String declCode = null;
		String classCode = null;

		for (Node dec : n.declist)
			declCode = nlJoin(declCode, visit(dec));
		return nlJoin("push 0", classCode, declCode, // generate code for declarations (allocation)
				visit(n.exp), "halt", getCode());
	}

	@Override
	public String visitNode(MethodNode n) throws VoidException {
		if (print)
			printNode(n, n.id);
		String declCode = null, popDecl = null, popParl = null;

		for (DecNode dec : n.declist) {
			declCode = nlJoin(declCode, visit(dec));
			popParl = dec.getType() instanceof ArrowTypeNode ? nlJoin(popParl, "pop", "pop") : nlJoin(popParl, "pop");
		}

		for (ParNode par : n.parlist)
			popParl = par.getType() instanceof ArrowTypeNode ? nlJoin(popParl, "pop", "pop") : nlJoin(popParl, "pop");

		n.label = freshFunLabel();
		putCode(nlJoin(n.label + ":", "cfp", // set $fp to $sp value
				"lra", // load $ra value
				declCode, // generate code for local declarations (they use the new $fp!!!)
				visit(n.exp), // generate code for function body expression
				"stm", // set $tm to popped value (function result)
				popDecl, // remove local declarations from stack
				"sra", // set $ra to popped value
				"pop", // remove Access Link from stack
				popParl, // remove parameters from stack
				"sfp", // set $fp to popped value (Control Link)
				"ltm", // load $tm value (function result)
				"lra", // load $ra value
				"js" // jump to to popped address
		));
		return null;
	}

	@Override
	public String visitNode(EmptyNode n) throws VoidException {
		if (print)
			printNode(n, "-1");
		return "push -1";
	}

	@Override
	public String visitNode(ClassCallNode n) throws VoidException {
		if (print)
			printNode(n, n.id);
		String argCode = null;
		for (int i = n.arguments.size() - 1; i >= 0; i--) {
			argCode = nlJoin(argCode, visit(n.arguments.get(i)));
		}
		String getAR = null;
		for (int i = 0; i < n.nestingLevel - n.entry.nl; i++) {
			getAR = nlJoin(getAR, "lw");
		}
		return nlJoin("lfp", argCode, "lfp", getAR, "push " + n.entry.offset, "add", "lw", "stm", "ltm", "ltm", "lw",
				"push " + n.methodEntry.offset, "add", "lw", "js");
	}

	@Override
	public String visitNode(ClassNode n) throws VoidException {
		List<String> newDispatchTable = n.superEntry != null
				? new ArrayList<>(dispatchTables.get(-n.superEntry.offset - 2))
				: new ArrayList<>();

		this.dispatchTables.add(newDispatchTable);

		n.methods.stream().peek(m -> visit(m)).forEach(m -> {

			if (m.offset < newDispatchTable.size()) {
				newDispatchTable.set(m.offset, m.label);
			} else {
				newDispatchTable.add(m.label);
			}
		});
		return nlJoin("lhp",
				newDispatchTable.stream().map(l -> nlJoin("push " + l, "lhp", "sw", "lhp", "push 1", "add", "shp"))
						.collect(Collectors.joining("\n")));
	}

	@Override
	public String visitNode(ProgNode n) {
		if (print)
			printNode(n);
		return nlJoin(visit(n.exp), "halt");
	}

	@Override
	public String visitNode(FunNode n) {
		if (print)
			printNode(n, n.id);
		String declCode = null, popDecl = null, popParl = null;
		for (DecNode dec : n.declist) {
			declCode = nlJoin(declCode, visit(dec));
			popDecl = dec.getType() instanceof ArrowTypeNode ? nlJoin(popDecl, "pop", "pop") : nlJoin(popDecl, "pop");
		}
		for (ParNode par : n.parlist) {
			popParl = par.getType() instanceof ArrowTypeNode ? nlJoin(popParl, "pop", "pop") : nlJoin(popParl, "pop");
		}
		String funl = freshFunLabel();
		putCode(nlJoin(funl + ":", "cfp", // set $fp to $sp value
				"lra", // load $ra value
				declCode, // generate code for local declarations (they use the new $fp!!!)
				visit(n.exp), // generate code for function body expression
				"stm", // set $tm to popped value (function result)
				popDecl, // remove local declarations from stack
				"sra", // set $ra to popped value
				"pop", // remove Access Link from stack
				popParl, // remove parameters from stack
				"sfp", // set $fp to popped value (Control Link)
				"ltm", // load $tm value (function result)
				"lra", // load $ra value
				"js" // jump to to popped address
		));
		return nlJoin("lfp", "push " + funl);
	}

	@Override
	public String visitNode(VarNode n) {
		if (print)
			printNode(n, n.id);
		return visit(n.exp);
	}

	@Override
	public String visitNode(PrintNode n) {
		if (print)
			printNode(n);
		return nlJoin(visit(n.exp), "print");
	}

	@Override
	public String visitNode(IfNode n) {
		if (print)
			printNode(n);
		String l1 = freshLabel();
		String l2 = freshLabel();
		return nlJoin(visit(n.cond), "push 1", "beq " + l1, visit(n.el), "b " + l2, l1 + ":", visit(n.th), l2 + ":");
	}

	@Override
	public String visitNode(EqualNode n) {
		if (print)
			printNode(n);
		String l1 = freshLabel();
		String l2 = freshLabel();
		return nlJoin(visit(n.left), visit(n.right), "beq " + l1, "push 0", "b " + l2, l1 + ":", "push 1", l2 + ":");
	}

	@Override
	public String visitNode(TimesNode n) {
		if (print)
			printNode(n);
		return nlJoin(visit(n.left), visit(n.right), "mult");
	}

	public String visitNode(DivNode n) {
		if (print)
			printNode(n);
		return nlJoin(visit(n.left), visit(n.right), "div");
	}

	@Override
	public String visitNode(PlusNode n) {
		if (print)
			printNode(n);
		return nlJoin(visit(n.left), visit(n.right), "add");
	}

	@Override
	public String visitNode(MinusNode n) {
		if (print)
			printNode(n);
		return nlJoin(visit(n.left), visit(n.right), "sub");
	}

	@Override
	public String visitNode(final GreaterEqualNode node) {
		if (print) {
			printNode(node);
		}
		String l1 = freshLabel();
		String l2 = freshLabel();
		String l3 = freshLabel();
		String left = visit(node.left);
		String right = visit(node.right);
		return nlJoin(left, right, "beq " + l1, left, right, "bleq " + l3, l1 + ":", "push 1", "b " + l2, l3 + ":",
				"push 0", l2 + ":");
	}

	public String visitNode(final LessEqualNode node) {
		if (print) {
			printNode(node);
		}
		String l1 = freshLabel();
		String l2 = freshLabel();
		return nlJoin(visit(node.left), visit(node.right), "bleq " + l1, "push 0", "b " + l2, l1 + ":", "push 1",
				l2 + ":");
	}

	public String visitNode(AndNode n) {
		if (print)
			printNode(n);
		String l1 = freshLabel();
		String l2 = freshLabel();
		return nlJoin(visit(n.left), visit(n.right), "mult", "push 1", "beq " + l1, "push 0", "b " + l2, l1 + ":",
				"push 1", l2 + ":");
	}

	public String visitNode(OrNode n) {
		if (print)
			printNode(n);
		String l1 = freshLabel();
		String l2 = freshLabel();
		return nlJoin(visit(n.left), visit(n.right), "add", "push 0", "bleq " + l1, "push 1", "b " + l2, l1 + ":",
				"push 0", l2 + ":");
	}

	public String visitNode(NotNode n) {
		if (print)
			printNode(n);
		String l1 = freshLabel();
		String l2 = freshLabel();
		return nlJoin(visit(n.exp), "push 0", "beq " + l1, "push 0", "b " + l2, l1 + ":", "push 1", l2 + ":");
	}

	@Override
	public String visitNode(CallNode n) {
		if (print)
			printNode(n, n.id);
		String argCode = null, getAR = null;
		for (int i = n.arglist.size() - 1; i >= 0; i--)
			argCode = nlJoin(argCode, visit(n.arglist.get(i)));
		for (int i = 0; i < n.nl - n.entry.nl; i++)
			getAR = nlJoin(getAR, "lw");

		if (n.entry.type instanceof MethodTypeNode) {
			return nlJoin("lfp", // load Control Link (pointer to frame of function "id" caller)
					argCode, // generate code for argument expressions in reversed order
					"lfp", getAR, // retrieve address of frame containing "id" declaration
									// by following the static chain (of Access Links)
					"stm", // set $tm to popped value (with the aim of duplicating top of stack)
					"ltm", // load Access Link (pointer to frame of function "id" declaration)
					"ltm", // duplicate top of stack
					"lw", "push " + n.entry.offset, "add", // compute address of "id" declaration
					"lw", // load address of "id" function
					"js" // jump to popped address (saving address of subsequent instruction in $ra)
			);
		} else {
			return nlJoin("lfp", // load Control Link (pointer to frame of function "id" caller)
					argCode, // generate code for argument expressions in reversed order
					"lfp", getAR, // retrieve address of frame containing "id" declaration
									// by following the static chain (of Access Links)
					"stm", // set $tm to popped value (with the aim of duplicating top of stack)
					"ltm", // load Access Link (pointer to frame of function "id" declaration)
					"push " + n.entry.offset, "add", // compute address of
					"lw", "ltm", // load address of "id" function
					"push " + (n.entry.offset - 1), "add", // compute address of
					"lw", // load address of "id" function
					"js");
		}

	}

	@Override
	public String visitNode(NewNode n) throws VoidException {
		String result = null;
		for (Node e : n.params) {
			result = nlJoin(result, visit(e)); // carico il codice dei parametri
		}
		for (Node e : n.params) {
			result = nlJoin(result, "lhp", "sw", // carico indirizzo di hp e metto il primo
					// valore nello heap
					"lhp", "push 1", "add", "shp"); // incremento hp
		}
		// scrivo ad indirizzo hp il contenuto di ExecuteVM.MEMSIZE + n.entry.offset
		result = nlJoin(result, "push " + (ExecuteVM.MEMSIZE), "push " + n.entry.offset, "add", "lw", "lhp", "sw");

		// carico il valore di hp e lo incremento
		result = nlJoin(result, "lhp", "lhp", "push 1", "add", "shp");
		return result;
	}

	@Override
	public String visitNode(IdNode n) {
		if (print)
			printNode(n, n.id);
		String getAR = null;
		for (int i = 0; i < n.nl - n.entry.nl; i++)
			getAR = nlJoin(getAR, "lw");
		if (n.entry.type instanceof ArrowTypeNode) {
			int funAddr = n.entry.offset - 1; // caso arrowtype: offset - 1
			return nlJoin("lfp", getAR, // retrieve address of frame containing "id" declaration
										// by following the static chain (of Access Links)
					"push " + n.entry.offset, "add", // compute address of "id" declaration
					"lw", // load value of "id" variable
					"lfp", getAR, // load address of "id" function
					"push " + funAddr, "add", // compute address of
					"lw" // load address of "id" function
			);

		} else {
			return nlJoin("lfp", getAR, // retrieve address of frame containing "id" declaration
					// by following the static chain (of Access Links)
					"push " + n.entry.offset, "add", // compute address of "id" declaration
					"lw" // load value of "id" variable
			);
		}
	}

	@Override
	public String visitNode(BoolNode n) {
		if (print)
			printNode(n, n.val.toString());
		return "push " + (n.val ? 1 : 0);
	}

	@Override
	public String visitNode(IntNode n) {
		if (print)
			printNode(n, n.val.toString());
		return "push " + n.val;
	}
}