package compiler;

import compiler.AST.AndNode;
import compiler.AST.ArrowTypeNode;
import compiler.AST.BoolNode;
import compiler.AST.BoolTypeNode;
import compiler.AST.CallNode;
import compiler.AST.ClassCallNode;
import compiler.AST.ClassNode;
import compiler.AST.ClassTypeNode;
import compiler.AST.DivNode;
import compiler.AST.EmptyNode;
import compiler.AST.EmptyTypeNode;
import compiler.AST.EqualNode;
import compiler.AST.FieldNode;
import compiler.AST.FunNode;
import compiler.AST.GreaterEqualNode;
import compiler.AST.IdNode;
import compiler.AST.IfNode;
import compiler.AST.IntNode;
import compiler.AST.IntTypeNode;
import compiler.AST.LessEqualNode;
import compiler.AST.MethodNode;
import compiler.AST.MethodTypeNode;
import compiler.AST.MinusNode;
import compiler.AST.NewNode;
import compiler.AST.NotNode;
import compiler.AST.OrNode;
import compiler.AST.ParNode;
import compiler.AST.PlusNode;
import compiler.AST.PrintNode;
import compiler.AST.ProgLetInNode;
import compiler.AST.ProgNode;
import compiler.AST.RefTypeNode;
import compiler.AST.TimesNode;
import compiler.AST.VarNode;
import compiler.exc.VoidException;
import compiler.lib.BaseEASTVisitor;
import compiler.lib.Node;

public class PrintEASTVisitor extends BaseEASTVisitor<Void, VoidException> {

	PrintEASTVisitor() {
		super(false, true);
	}

	@Override
	public Void visitNode(ProgLetInNode n) {
		printNode(n);
		for (Node dec : n.declist)
			visit(dec);
		visit(n.exp);
		return null;
	}

	@Override
	public Void visitNode(ProgNode n) {
		printNode(n);
		visit(n.exp);
		return null;
	}

	@Override
	public Void visitNode(FunNode n) {
		printNode(n, n.id);
		visit(n.retType);
		for (ParNode par : n.parlist)
			visit(par);
		for (Node dec : n.declist)
			visit(dec);
		visit(n.exp);
		return null;
	}

	@Override
	public Void visitNode(ParNode n) {
		printNode(n, n.id);
		visit(n.getType());
		return null;
	}

	@Override
	public Void visitNode(VarNode n) {
		printNode(n, n.id);
		visit(n.getType());
		visit(n.exp);
		return null;
	}

	@Override
	public Void visitNode(PrintNode n) {
		printNode(n);
		visit(n.exp);
		return null;
	}

	@Override
	public Void visitNode(IfNode n) {
		printNode(n);
		visit(n.cond);
		visit(n.th);
		visit(n.el);
		return null;
	}

	@Override
	public Void visitNode(EqualNode n) {
		printNode(n);
		visit(n.left);
		visit(n.right);
		return null;
	}

	@Override
	public Void visitNode(final GreaterEqualNode node) {
		printNode(node);
		visit(node.left);
		visit(node.right);
		return null;
	}

	@Override
	public Void visitNode(final LessEqualNode node) {
		printNode(node);
		visit(node.left);
		visit(node.right);
		return null;
	}

	@Override
	public Void visitNode(TimesNode n) {
		printNode(n);
		visit(n.left);
		visit(n.right);
		return null;
	}

	@Override
	public Void visitNode(final DivNode node) {
		printNode(node);
		visit(node.left);
		visit(node.right);
		return null;
	}

	@Override
	public Void visitNode(PlusNode n) {
		printNode(n);
		visit(n.left);
		visit(n.right);
		return null;
	}

	@Override
	public Void visitNode(final MinusNode node) {
		printNode(node);
		visit(node.left);
		visit(node.right);
		return null;
	}

	@Override
	public Void visitNode(final AndNode node) {
		printNode(node);
		visit(node.left);
		visit(node.right);
		return null;
	}

	@Override
	public Void visitNode(final OrNode node) {
		printNode(node);
		visit(node.left);
		visit(node.right);
		return null;
	}

	@Override
	public Void visitNode(final NotNode node) {
		printNode(node);
		visit(node.exp);
		return null;
	}

	@Override
	public Void visitNode(CallNode n) {
		printNode(n, n.id + " at nestinglevel " + n.nl);
		visit(n.entry);
		for (Node arg : n.arglist)
			visit(arg);
		return null;
	}

	@Override
	public Void visitNode(IdNode n) {
		printNode(n, n.id + " at nestinglevel " + n.nl);
		visit(n.entry);
		return null;
	}

	@Override
	public Void visitNode(BoolNode n) {
		printNode(n, n.val.toString());
		return null;
	}

	@Override
	public Void visitNode(IntNode n) {
		printNode(n, n.val.toString());
		return null;
	}

	@Override
	public Void visitNode(final ClassNode node) throws VoidException {
		printNode(node, node.id);
		visit(node.getType());
		visit(node.superEntry);
		for (final var field : node.fields) {
			visit(field);
		}
		for (final var method : node.methods) {
			visit(method);
		}
		return null;
	}

	@Override
	public Void visitNode(final FieldNode node) throws VoidException {
		printNode(node, node.id);
		visit(node.getType());
		return null;
	}

	@Override
	public Void visitNode(final MethodNode node) throws VoidException {
		printNode(node, node.id);
		visit(node.getType());
		visit(node.retType);
		for (final var par : node.parlist) {
			visit(par);
		}
		for (final var dec : node.declist) {
			visit(dec);
		}
		visit(node.exp);
		return null;
	}

	@Override
	public Void visitNode(final ClassCallNode node) throws VoidException {
		printNode(node, node.id);
		visit(node.entry);
		visit(node.methodEntry);
		for (final var arg : node.arguments) {
			visit(arg);
		}
		return null;
	}

	@Override
	public Void visitNode(final NewNode node) throws VoidException {
		printNode(node, node.id);
		visit(node.entry);
		for (final var arg : node.params) {
			visit(arg);
		}
		return null;
	}

	@Override
	public Void visitNode(final EmptyNode node) throws VoidException {
		printNode(node);
		return null;
	}

	@Override
	public Void visitNode(ArrowTypeNode n) {
		printNode(n);
		for (Node par : n.parlist)
			visit(par);
		visit(n.ret, "->"); // marks return type
		return null;
	}

	@Override
	public Void visitNode(BoolTypeNode n) {
		printNode(n);
		return null;
	}

	@Override
	public Void visitNode(IntTypeNode n) {
		printNode(n);
		return null;
	}

	@Override
	public Void visitNode(final ClassTypeNode node) throws VoidException {
		printNode(node);
		for (final var field : node.allFields) {
			visit(field);
		}
		for (final var method : node.allMethods) {
			visit(method);
		}
		return null;
	}

	@Override
	public Void visitNode(final MethodTypeNode node) throws VoidException {
		printNode(node);
		visit(node.fun);
		return null;
	}

	@Override
	public Void visitNode(final RefTypeNode node) throws VoidException {
		printNode(node, node.id);
		return null;
	}

	@Override
	public Void visitNode(final EmptyTypeNode node) throws VoidException {
		printNode(node);
		return null;
	}

	@Override
	public Void visitSTentry(STentry entry) {
		printSTentry("nestlev " + entry.nl);
		printSTentry("type");
		visit(entry.type);
		printSTentry("offset " + entry.offset);
		return null;
	}
}
