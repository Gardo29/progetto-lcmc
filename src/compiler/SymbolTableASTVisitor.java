package compiler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import compiler.AST.AndNode;
import compiler.AST.ArrowTypeNode;
import compiler.AST.BoolNode;
import compiler.AST.CallNode;
import compiler.AST.ClassCallNode;
import compiler.AST.ClassNode;
import compiler.AST.ClassTypeNode;
import compiler.AST.DivNode;
import compiler.AST.EmptyNode;
import compiler.AST.EqualNode;
import compiler.AST.FieldNode;
import compiler.AST.FunNode;
import compiler.AST.GreaterEqualNode;
import compiler.AST.IdNode;
import compiler.AST.IfNode;
import compiler.AST.IntNode;
import compiler.AST.LessEqualNode;
import compiler.AST.MethodNode;
import compiler.AST.MethodTypeNode;
import compiler.AST.MinusNode;
import compiler.AST.NewNode;
import compiler.AST.NotNode;
import compiler.AST.OrNode;
import compiler.AST.ParNode;
import compiler.AST.PlusNode;
import compiler.AST.PrintNode;
import compiler.AST.ProgLetInNode;
import compiler.AST.ProgNode;
import compiler.AST.RefTypeNode;
import compiler.AST.TimesNode;
import compiler.AST.VarNode;
import compiler.exc.VoidException;
import compiler.lib.BaseASTVisitor;
import compiler.lib.Node;
import compiler.lib.TypeNode;

public class SymbolTableASTVisitor extends BaseASTVisitor<Void, VoidException> {

	private List<Map<String, STentry>> symTable = new ArrayList<>();
	private Map<String, Map<String, STentry>> classTable = new HashMap<>();

	private int nestingLevel = 0; // current nesting level
	private int decOffset = -2; // counter for offset of local declarations at current nesting level
	int stErrors = 0;

	SymbolTableASTVisitor() {
	}

	SymbolTableASTVisitor(boolean debug) {
		super(debug);
	} // enables print for debugging

	private STentry stLookup(String id) {
		int j = nestingLevel;
		STentry entry = null;
		while (j >= 0 && entry == null)
			entry = symTable.get(j--).get(id);
		return entry;
	}

	@Override
	public Void visitNode(ProgLetInNode n) {
		if (print)
			printNode(n);
		Map<String, STentry> hm = new HashMap<>();
		symTable.add(hm);
		n.declist.forEach(dec -> {
			visit(dec);
		});
		visit(n.exp);
		symTable.remove(0);
		return null;
	}

	@Override
	public Void visitNode(ProgNode n) {
		if (print)
			printNode(n);
		visit(n.exp);
		return null;
	}

	@Override
	public Void visitNode(ClassNode n) {
		if (print)
			printNode(n);

		Map<String, STentry> level0 = symTable.get(nestingLevel); // livello 0 della symbolTable
		STentry superEntry = level0.get(n.superID);
		if (n.superID != null && superEntry == null) {
			System.out.println("Extended class id " + n.superID + " at line " + n.getLine() + " not declared");
			stErrors++;
		}

		// creo le cose base
		ClassTypeNode classTypeNode = new ClassTypeNode(new ArrayList<>(), new ArrayList<>());
		Map<String, STentry> newVirtualTable = new HashMap<>(); // nuova vt

		if (n.superID != null && superEntry != null) { // copio le virtual table e aggiorno il class type
			// serve per la copia successiva delle informazioni della classe madre
			n.superEntry = superEntry;
			ClassTypeNode classTypeNodeMother = (ClassTypeNode) superEntry.type;
			classTypeNode.allMethods.addAll(classTypeNodeMother.allMethods);
			classTypeNode.allFields.addAll(classTypeNodeMother.allFields);
			newVirtualTable = new HashMap<>(classTable.get(n.superID));
		}
		// inserimento classe nella symtable
		if (level0.put(n.id, new STentry(nestingLevel, classTypeNode, decOffset)) != null) {
			System.out.println("Class id " + n.id + " at line " + n.getLine() + " already declared");
			stErrors++;
		}
		decOffset--;
		nestingLevel++; // entro dentro il corpo della classe
		Map<String, STentry> newSymTable = newVirtualTable;
		// inserimento contenuto classe nella classTable e nella sybleTable attuale
		classTable.put(n.id, newSymTable);
		symTable.add(newSymTable);

		// variabili per indice e check su ridichiarazioni
		int fieldIndex = -classTypeNode.allFields.size() - 1;
		Set<String> fieldsName = new HashSet<>();

		for (FieldNode field : n.fields) { // aggiungo i fields e controllo che non ci siano duplicati
			if (!fieldsName.add(field.id)) {
				System.out.println("Field id " + field.id + " at line " + field.getLine() + " already declared");
				stErrors++;
			} else {
				if (newSymTable.containsKey(field.id)) {
					if (newSymTable.get(field.id).type instanceof MethodTypeNode) {
						System.out.println(
								"You can't override method with field id " + field.id + " at line " + field.getLine());
						stErrors++;
					} else {
						final int offset = newSymTable.get(field.id).offset; // prendo l'offset di quello
																				// vecchio (override)
						field.offset = offset;
						newSymTable.put(field.id, new STentry(nestingLevel, field.getType(), offset));
						classTypeNode.allFields.set(-offset - 1, field.getType());
					}
				} else {
					field.offset = fieldIndex;
					newSymTable.put(field.id, new STentry(nestingLevel, field.getType(), fieldIndex));
					classTypeNode.allFields.add(-fieldIndex - 1, field.getType());
					fieldIndex--;
				}
			}

		}
		// variabili per indice
		int saveOffset = decOffset;
		decOffset = classTypeNode.allMethods.size();
		Set<String> methodsName = new HashSet<>();

		for (MethodNode m : n.methods) {
			if (!methodsName.add(m.id)) {
				System.out.println("Method id " + m.id + " at line " + m.getLine() + " already declared");
				stErrors++;
			}
			visit(m);
			if (m.offset >= classTypeNode.allMethods.size()) {
				classTypeNode.allMethods.add(((MethodTypeNode) m.getType()).fun);
			} else {
				classTypeNode.allMethods.set(m.offset, ((MethodTypeNode) m.getType()).fun);
			}
		}
		// rimuovere la hashmap corrente poiche' esco dallo scope
		decOffset = saveOffset;
		symTable.remove(nestingLevel--);
		n.setType(classTypeNode);
		return null;
	}

	@Override
	public Void visitNode(ClassCallNode node) throws VoidException {

		STentry refType = stLookup(node.id);

		if (refType == null) {
			System.out.println("Object id " + node.id + " at line " + node.getLine() + " not declared");
			stErrors++;

		} else {
			node.entry = refType;
			node.nestingLevel = nestingLevel;
			if (!(refType.type instanceof RefTypeNode)) {
				System.out.println("Uncorrect use of id " + node.id + " with type "
						+ refType.type.getClass().getSimpleName() + " at line " + node.getLine());
				stErrors++;
			} else {
				Map<String, STentry> virtualTable = classTable.get(((RefTypeNode) refType.type).id);
				if (virtualTable == null) {
					System.out.println("Type " + node.id + " not valid");
					stErrors++;
				} else {
					node.methodEntry = virtualTable.get(node.methodId);
					if (node.methodEntry == null) {
						System.out.println("Method id " + node.id + " at line " + node.getLine() + " not declared");
						stErrors++;
						return null;
					}
				}

			}

		}
		node.arguments.forEach(a -> visit(a));
		return null;
	}

	@Override
	public Void visitNode(NewNode n) throws VoidException {

		STentry classNew = symTable.get(0).get(n.id);

		if (classNew == null) {
			System.out.println("Constructor name " + n.id + " at line " + n.getLine() + " not declared");
			stErrors++;
		}
		n.entry = classNew;
		n.nl = nestingLevel;
		n.params.forEach(p -> visit(p));
		return null;
	}

	@Override
	public Void visitNode(MethodNode n) {
		if (print)
			printNode(n);
		Map<String, STentry> hm = symTable.get(nestingLevel);

		List<TypeNode> parTypes = n.parlist.stream().map(p -> p.getType()).collect(Collectors.toList());
		n.setType(new MethodTypeNode(new ArrowTypeNode(parTypes, n.retType)));

		// inserimento di ID nella symtable
		if (hm.containsKey(n.id)) {
			final STentry oldMethod = hm.get(n.id);
			if (!(oldMethod.type instanceof MethodTypeNode)) {
				System.out.println("You can't override fields with methods, id " + n.id + " at line " + n.getLine());
				stErrors++;
			} else {
				hm.put(n.id, new STentry(nestingLevel, n.getType(), oldMethod.offset));
				n.offset = oldMethod.offset;
			}
		} else {
			n.offset = decOffset++;
			hm.put(n.id, new STentry(nestingLevel, n.getType(), n.offset));
		}
		// creare una nuova hashmap per la symTable
		nestingLevel++;
		Map<String, STentry> hmn = new HashMap<>();
		symTable.add(hmn);
		int prevNLDecOffset = decOffset; // stores counter for offset of declarations at previous nesting level
		decOffset = -2;
		int parOffset = 1;

		for (ParNode par : n.parlist) {
			final TypeNode parType = par.getType();
			if (parType instanceof ArrowTypeNode) {
				parOffset++;
			}
			if (hmn.put(par.id, new STentry(nestingLevel, parType, parOffset++)) != null) {
				System.out.println("Parameter id " + par.id + " at line " + n.getLine() + " already declared");
				stErrors++;
			}
		}

		for (Node dec : n.declist)
			visit(dec);
		visit(n.exp);
		// rimuovere la hashmap corrente poiche' esco dallo scope
		symTable.remove(nestingLevel--);
		decOffset = prevNLDecOffset; // restores counter for offset of declarations at previous nesting level
		return null;
	}

	@Override
	public Void visitNode(FunNode n) {
		if (print)
			printNode(n);
		Map<String, STentry> hm = symTable.get(nestingLevel);
		List<TypeNode> parTypes = new ArrayList<>();
		for (ParNode par : n.parlist) {
			parTypes.add(par.getType());
		}
		ArrowTypeNode arr = new ArrowTypeNode(parTypes, n.retType);
		STentry entry = new STentry(nestingLevel, arr, decOffset);
		// High Order
		decOffset = decOffset - 2;
		n.setType(arr);
		// inserimento di ID nella symtable
		if (hm.put(n.id, entry) != null) {
			System.out.println("Fun id " + n.id + " at line " + n.getLine() + " already declared");
			stErrors++;
		}
		// creare una nuova hashmap per la symTable
		nestingLevel++;
		Map<String, STentry> hmn = new HashMap<>();
		symTable.add(hmn);
		int prevNLDecOffset = decOffset; // stores counter for offset of declarations at previous nesting level
		decOffset = -2;

		int parOffset = 0;
		for (ParNode par : n.parlist) {
			// Se è ArrowTypeNode aggiungo offset di 2, altrimenti 1
			if (par.getType() instanceof ArrowTypeNode) {
				parOffset += 2;
			} else {
				parOffset++;
			}
			if (hmn.put(par.id, new STentry(nestingLevel, par.getType(), parOffset)) != null) {
				System.out.println("Par id " + par.id + " at line " + n.getLine() + " already declared");
				stErrors++;
			}

		}
		for (Node dec : n.declist)
			visit(dec);
		visit(n.exp);
		// rimuovere la hashmap corrente poiche' esco dallo scope
		symTable.remove(nestingLevel--);
		decOffset = prevNLDecOffset; // restores counter for offset of declarations at previous nesting level
		return null;
	}

	@Override
	public Void visitNode(VarNode n) {
		if (print)
			printNode(n);
		visit(n.exp);
		Map<String, STentry> hm = symTable.get(nestingLevel);
		STentry entry = new STentry(nestingLevel, n.getType(), decOffset--);
		// inserimento di ID nella symtable
		if (hm.put(n.id, entry) != null) {
			System.out.println("Var id " + n.id + " at line " + n.getLine() + " already declared");
			stErrors++;
		}
		return null;
	}

	@Override
	public Void visitNode(PrintNode n) {
		if (print)
			printNode(n);
		visit(n.exp);
		return null;
	}

	@Override
	public Void visitNode(IfNode n) {
		if (print)
			printNode(n);
		visit(n.cond);
		visit(n.th);
		visit(n.el);
		return null;
	}

	@Override
	public Void visitNode(EqualNode n) {
		if (print)
			printNode(n);
		visit(n.left);
		visit(n.right);
		return null;
	}

	@Override
	public Void visitNode(TimesNode n) {
		if (print)
			printNode(n);
		visit(n.left);
		visit(n.right);
		return null;
	}

	@Override
	public Void visitNode(DivNode n) {
		if (print)
			printNode(n);
		visit(n.left);
		visit(n.right);
		return null;
	}

	@Override
	public Void visitNode(PlusNode n) {
		if (print)
			printNode(n);
		visit(n.left);
		visit(n.right);
		return null;
	}

	@Override
	public Void visitNode(MinusNode n) {
		if (print)
			printNode(n);
		visit(n.left);
		visit(n.right);
		return null;
	}

	@Override
	public Void visitNode(GreaterEqualNode n) {
		if (print)
			printNode(n);
		visit(n.left);
		visit(n.right);
		return null;
	}

	@Override
	public Void visitNode(LessEqualNode n) {
		if (print)
			printNode(n);
		visit(n.left);
		visit(n.right);
		return null;
	}

	@Override
	public Void visitNode(AndNode n) {
		if (print)
			printNode(n);
		visit(n.left);
		visit(n.right);
		return null;
	}

	@Override
	public Void visitNode(OrNode n) {
		if (print)
			printNode(n);
		visit(n.left);
		visit(n.right);
		return null;
	}

	@Override
	public Void visitNode(NotNode n) {
		if (print)
			printNode(n);
		visit(n.exp);
		return null;
	}

	@Override
	public Void visitNode(CallNode n) {
		if (print)
			printNode(n);
		STentry entry = stLookup(n.id);
		if (entry == null) {
			System.out.println("Fun id " + n.id + " at line " + n.getLine() + " not declared");
			stErrors++;
		} else {
			n.entry = entry;
			n.nl = nestingLevel;
		}
		for (Node arg : n.arglist)
			visit(arg);
		return null;
	}

	@Override
	public Void visitNode(IdNode n) {
		if (print)
			printNode(n);
		STentry entry = stLookup(n.id);
		if (entry == null) {
			System.out.println("Var or Par id " + n.id + " at line " + n.getLine() + " not declared");
			stErrors++;
		} else {
			n.entry = entry;
			n.nl = nestingLevel;
		}
		return null;
	}

	@Override
	public Void visitNode(EmptyNode n) throws VoidException {
		if (print)
			printNode(n, "null");
		return null;
	}

	@Override
	public Void visitNode(BoolNode n) {
		if (print)
			printNode(n, n.val.toString());
		return null;
	}

	@Override
	public Void visitNode(IntNode n) {
		if (print)
			printNode(n, n.val.toString());
		return null;
	}
}
