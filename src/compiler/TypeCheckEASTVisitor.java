package compiler;

import static compiler.TypeRels.isSubtype;

import java.util.List;

import compiler.AST.AndNode;
import compiler.AST.ArrowTypeNode;
import compiler.AST.BoolNode;
import compiler.AST.BoolTypeNode;
import compiler.AST.CallNode;
import compiler.AST.ClassCallNode;
import compiler.AST.ClassNode;
import compiler.AST.ClassTypeNode;
import compiler.AST.DivNode;
import compiler.AST.EmptyNode;
import compiler.AST.EmptyTypeNode;
import compiler.AST.EqualNode;
import compiler.AST.FieldNode;
import compiler.AST.FunNode;
import compiler.AST.GreaterEqualNode;
import compiler.AST.IdNode;
import compiler.AST.IfNode;
import compiler.AST.IntNode;
import compiler.AST.IntTypeNode;
import compiler.AST.LessEqualNode;
import compiler.AST.MethodNode;
import compiler.AST.MethodTypeNode;
import compiler.AST.MinusNode;
import compiler.AST.NewNode;
import compiler.AST.NotNode;
import compiler.AST.OrNode;
import compiler.AST.PlusNode;
import compiler.AST.PrintNode;
import compiler.AST.ProgLetInNode;
import compiler.AST.ProgNode;
import compiler.AST.RefTypeNode;
import compiler.AST.TimesNode;
import compiler.AST.VarNode;
import compiler.exc.IncomplException;
import compiler.exc.TypeException;
import compiler.lib.BaseEASTVisitor;
import compiler.lib.Node;
import compiler.lib.TypeNode;

//visitNode(n) fa il type checking di un Node n e ritorna:
//- per una espressione, il suo tipo (oggetto BoolTypeNode o IntTypeNode)
//- per una dichiarazione, "null"; controlla la correttezza interna della dichiarazione
//(- per un tipo: "null"; controlla che il tipo non sia incompleto) 
//
//visitSTentry(s) ritorna, per una STentry s, il tipo contenuto al suo interno
public class TypeCheckEASTVisitor extends BaseEASTVisitor<TypeNode, TypeException> {

	TypeCheckEASTVisitor() {
		super(true);
	} // enables incomplete tree exceptions

	TypeCheckEASTVisitor(boolean debug) {
		super(true, debug);
	} // enables print for debugging

	// checks that a type object is visitable (not incomplete)
	private TypeNode ckvisit(TypeNode t) throws TypeException {
		visit(t);
		return t;
	}

	@Override
	public TypeNode visitNode(RefTypeNode n) throws TypeException {
		if (print)
			printNode(n);
		return null;
	}

	@Override
	public TypeNode visitNode(ProgLetInNode n) throws TypeException {
		if (print)
			printNode(n);
		for (Node dec : n.declist)
			try {
				visit(dec);
			} catch (IncomplException e) {
			} catch (TypeException e) {
				System.out.println("Type checking error in a declaration: " + e.text);
			}
		return visit(n.exp);
	}

	@Override
	public TypeNode visitNode(ProgNode n) throws TypeException {
		if (print)
			printNode(n);
		return visit(n.exp);
	}

	@Override
	public TypeNode visitNode(FunNode n) throws TypeException {
		if (print)
			printNode(n, n.id);
		for (Node dec : n.declist)
			try {
				visit(dec);
			} catch (IncomplException e) {
			} catch (TypeException e) {
				System.out.println("Type checking error in a declaration: " + e.text);
			}
		if (!isSubtype(visit(n.exp), ckvisit(n.retType)))
			throw new TypeException("Wrong return type for function " + n.id, n.getLine());
		return null;
	}

	@Override
	public TypeNode visitNode(VarNode n) throws TypeException {
		if (print)
			printNode(n, n.id);
		if (!isSubtype(visit(n.exp), ckvisit(n.getType()))) {
			throw new TypeException("Incompatible value for variable " + n.id, n.getLine());
		}
		return null;
	}

	@Override
	public TypeNode visitNode(PrintNode n) throws TypeException {
		if (print)
			printNode(n);
		return visit(n.exp);
	}

	@Override
	public TypeNode visitNode(IfNode n) throws TypeException {
		if (print)
			printNode(n);
		if (!(isSubtype(visit(n.cond), new BoolTypeNode())))
			throw new TypeException("Non boolean condition in if", n.getLine());
		TypeNode t = visit(n.th);
		TypeNode e = visit(n.el);

		try {
			TypeNode returnType = TypeRels.lowestCommonAncestor(t, e);

			if (returnType != null) {
				return returnType;
			}
			throw new TypeException("Incompatible types in then-else branches", n.getLine());
		} catch (NullPointerException b) {
			b.printStackTrace();
		}
		return null;
	}

	@Override
	public TypeNode visitNode(EqualNode n) throws TypeException {
		if (print)
			printNode(n);
		TypeNode l = visit(n.left);
		TypeNode r = visit(n.right);
		// No espressioni con funzioni
		if (l instanceof ArrowTypeNode || r instanceof ArrowTypeNode)
			throw new TypeException("Equal is not compatible with functions", n.getLine());
		if (!(isSubtype(l, r) || isSubtype(r, l)))
			throw new TypeException("Incompatible types in equal", n.getLine());
		return new BoolTypeNode();
	}

	@Override
	public TypeNode visitNode(TimesNode n) throws TypeException {
		if (print)
			printNode(n);
		if (!(isSubtype(visit(n.left), new IntTypeNode()) && isSubtype(visit(n.right), new IntTypeNode())))
			throw new TypeException("Non integers in multiplication", n.getLine());
		return new IntTypeNode();
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////

	public TypeNode visitNode(DivNode n) throws TypeException {
		if (print)
			printNode(n);
		if (!(isSubtype(visit(n.left), new IntTypeNode()) && isSubtype(visit(n.right), new IntTypeNode())))
			throw new TypeException("Non integers in divide", n.getLine());
		return new IntTypeNode();
	}

	@Override
	public TypeNode visitNode(PlusNode n) throws TypeException {
		if (print)
			printNode(n);
		if (!(isSubtype(visit(n.left), new IntTypeNode()) && isSubtype(visit(n.right), new IntTypeNode())))
			throw new TypeException("Non integers in sum", n.getLine());
		return new IntTypeNode();
	}

	@Override
	public TypeNode visitNode(MinusNode n) throws TypeException {
		if (print)
			printNode(n);
		if (!(isSubtype(visit(n.left), new IntTypeNode()) && isSubtype(visit(n.right), new IntTypeNode())))
			throw new TypeException("Non integers in minus", n.getLine());
		return new IntTypeNode();
	}

	@Override
	public TypeNode visitNode(GreaterEqualNode n) throws TypeException {
		if (print)
			printNode(n);
		if (!(isSubtype(visit(n.left), new IntTypeNode()) && isSubtype(visit(n.right), new IntTypeNode())))
			throw new TypeException("Incompatible types in GreaterEqual", n.getLine());
		return new BoolTypeNode();
	}

	@Override
	public TypeNode visitNode(LessEqualNode n) throws TypeException {
		if (print)
			printNode(n);
		if (!(isSubtype(visit(n.left), new IntTypeNode()) && isSubtype(visit(n.right), new IntTypeNode())))
			throw new TypeException("Incompatible types in LessEqual", n.getLine());
		return new BoolTypeNode();
	}

	@Override
	public TypeNode visitNode(AndNode n) throws TypeException {
		if (print)
			printNode(n);
		TypeNode l = visit(n.left);
		TypeNode r = visit(n.right);
		if (!(isSubtype(l, new BoolTypeNode()) || isSubtype(r, new BoolTypeNode())))
			throw new TypeException("Incompatible types in AND", n.getLine());
		return new BoolTypeNode();
	}

	@Override
	public TypeNode visitNode(OrNode n) throws TypeException {
		if (print)
			printNode(n);
		TypeNode l = visit(n.left);
		TypeNode r = visit(n.right);
		if (!(isSubtype(l, new BoolTypeNode()) || isSubtype(r, new BoolTypeNode())))
			throw new TypeException("Incompatible types in OR", n.getLine());
		return new BoolTypeNode();
	}

	@Override
	public TypeNode visitNode(NotNode n) throws TypeException {
		if (print)
			printNode(n);
		TypeNode e = visit(n.exp);
		if (!isSubtype(e, new BoolTypeNode())) {
			throw new TypeException("Incompatible types in NOT", n.getLine());
		}
		return new BoolTypeNode();
	}

	@Override
	public TypeNode visitNode(ClassTypeNode n) throws TypeException {
		if (print)
			printNode(n);
		return null;
	}

	/*----------------------------------ESTENSIONE AD OGGETTI----------------------------------*/

	@Override
	public TypeNode visitNode(ClassNode n) throws TypeException {
		if (print)
			printNode(n);

		for (Node methods : n.methods) {
			visit(methods);
		}

		if (n.superID != null && n.superEntry == null) {
			System.out.println("Classe fallata" + n.id);
		}

		if (n.superID != null && n.superEntry != null) {
			TypeRels.superType.put(n.id, n.superID); // serve per il controllo successivo del tipo
			ClassTypeNode motherClass = (ClassTypeNode) n.superEntry.type;
			ClassTypeNode currentClass = (ClassTypeNode) n.getType();
			// si controlla l'override
			for (final FieldNode field : n.fields) {
				final int offset = -field.offset - 1;
				if (offset < motherClass.allFields.size()
						&& !isSubtype(currentClass.allFields.get(offset), motherClass.allFields.get(offset))) {
					throw new TypeException("Wrong override type for field " + field.id, field.getLine());
				}
			}
			for (final MethodNode method : n.methods) {
				final int offset = method.offset;
				if (offset < motherClass.allMethods.size()
						&& !isSubtype(currentClass.allMethods.get(offset), motherClass.allMethods.get(offset))) {
					System.out.println(currentClass.allMethods.get(offset) + "  " + motherClass.allMethods.get(offset));
					throw new TypeException("Wrong override type for method " + method.id, n.getLine());
				}
			}
		}
		return null;
	}

	@Override
	public TypeNode visitNode(MethodNode n) throws TypeException {
		if (print)
			printNode(n, n.id);
		for (Node dec : n.declist)
			try {
				visit(dec);
			} catch (IncomplException e) {
			} catch (TypeException e) {
				System.out.println("Type checking error in a declaration: " + e.text);
			}
		if (!isSubtype(visit(n.exp), ckvisit(n.retType)))
			throw new TypeException("Wrong return type for method " + n.id, n.getLine());
		return null;
	}

	@Override
	public TypeNode visitNode(ClassCallNode n) throws TypeException {
		if (print)
			printNode(n, n.id);
		final Node type = visit(n.methodEntry);
		if (!(type instanceof MethodTypeNode)) {
			throw new TypeException("Invocation of a non-method on " + n.id, n.getLine());
		}
		final ArrowTypeNode arrowType = ((MethodTypeNode) type).fun;
		if (arrowType.parlist.size() != n.arguments.size()) {
			throw new TypeException("Wrong number of parameters in the invocation of " + n.id + "." + n.methodId,
					n.getLine());
		}
		for (int i = 0; i < n.arguments.size(); i++) {
			if (!isSubtype(visit(n.arguments.get(i)), arrowType.parlist.get(i))) {
				throw new TypeException("Wrong type for " + (i + 1) + "-th parameter in the invocation of " + n.id + "."
						+ n.methodId + "()", n.getLine());
			}
		}
		return arrowType.ret;
	}

	@Override
	public TypeNode visitNode(NewNode n) throws TypeException {
		if (print)
			printNode(n, n.id);
		TypeNode t = visit(n.entry);
		if (!(t instanceof ClassTypeNode))
			throw new TypeException("Invalid constructor " + n.id, n.getLine());

		List<TypeNode> params = ((ClassTypeNode) n.entry.type).allFields;
		if (!(params.size() == n.params.size()))
			throw new TypeException("Wrong number of parameters in the invocation of " + n.id + ", required "
					+ params.size() + " but used " + n.params.size(), n.getLine());

		for (int i = 0; i < n.params.size(); i++)
			if (!(isSubtype(visit(n.params.get(i)), params.get(i))))
				throw new TypeException("Wrong type for " + (i + 1) + "-th parameter in the invocation of " + n.id,
						n.getLine());
		return new RefTypeNode(n.id);
	}

	@Override
	public TypeNode visitNode(EmptyNode n) throws TypeException {
		return new EmptyTypeNode();
	}
	/*--------------------------------------------------------------------------------------------*/

	@Override
	public TypeNode visitNode(CallNode n) throws TypeException {
		if (print)
			printNode(n, n.id);
		TypeNode t = visit(n.entry);
		if (!(t instanceof ArrowTypeNode || t instanceof MethodTypeNode))
			throw new TypeException("Invocation of a non-function " + n.id, n.getLine());

		ArrowTypeNode at = t instanceof MethodTypeNode ? ((MethodTypeNode) t).fun : (ArrowTypeNode) t;

		if (!(at.parlist.size() == n.arglist.size()))
			throw new TypeException("Wrong number of parameters in the invocation of " + n.id, n.getLine());
		for (int i = 0; i < n.arglist.size(); i++)
			if (!(isSubtype(visit(n.arglist.get(i)), at.parlist.get(i))))
				throw new TypeException("Wrong type for " + (i + 1) + "-th parameter in the invocation of " + n.id,
						n.getLine());
		return at.ret;
	}

	@Override
	public TypeNode visitNode(IdNode n) throws TypeException {
		if (print)
			printNode(n, n.id);
		TypeNode t = visit(n.entry);
		if (t instanceof ClassTypeNode || t instanceof MethodTypeNode)
			throw new TypeException("Wrong usage of identifier " + n.id, n.getLine());
		return t;
	}

	@Override
	public TypeNode visitNode(BoolNode n) {
		if (print)
			printNode(n, n.val.toString());
		return new BoolTypeNode();
	}

	@Override
	public TypeNode visitNode(IntNode n) {
		if (print)
			printNode(n, n.val.toString());
		return new IntTypeNode();
	}

	@Override
	public TypeNode visitNode(ArrowTypeNode n) throws TypeException {
		if (print)
			printNode(n);
		for (Node par : n.parlist)
			visit(par);
		visit(n.ret, "->"); // marks return type
		return null;
	}

	@Override
	public TypeNode visitNode(MethodTypeNode n) throws TypeException {
		if (print)
			printNode(n);
		visit(n.fun);
		return null;
	}

	@Override
	public TypeNode visitNode(BoolTypeNode n) {
		if (print)
			printNode(n);
		return null;
	}

	@Override
	public TypeNode visitNode(IntTypeNode n) {
		if (print)
			printNode(n);
		return null;
	}

// STentry (ritorna campo type)

	@Override
	public TypeNode visitSTentry(STentry entry) throws TypeException {
		if (print)
			printSTentry("type");
		return ckvisit(entry.type);
	}

}