package compiler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import compiler.AST.ArrowTypeNode;
import compiler.AST.BoolTypeNode;
import compiler.AST.EmptyTypeNode;
import compiler.AST.IntTypeNode;
import compiler.AST.RefTypeNode;
import compiler.lib.TypeNode;

public class TypeRels {
	static Map<String, String> superType = new HashMap<>();

	// valuta se il tipo "a" e' <= al tipo "b", dove "a" e "b" sono tipi di base:
	// IntTypeNode o BoolTypeNode
	public static boolean isSubtype(TypeNode a, TypeNode b) {
		return baseSubtypes(a, b) || superType(a, b) || checkFunctions(a, b);
	}

	public static TypeNode lowestCommonAncestor(TypeNode a, TypeNode b) {
		if (a instanceof IntTypeNode && b instanceof IntTypeNode) {
			return new IntTypeNode();
		}
		if (a instanceof BoolTypeNode && b instanceof BoolTypeNode) {
			return new BoolTypeNode();
		}
		if (a instanceof EmptyTypeNode && b instanceof EmptyTypeNode) {
			new EmptyTypeNode();
		}
		if (a instanceof EmptyTypeNode && b instanceof RefTypeNode) {
			return b;
		}

		if (b instanceof EmptyTypeNode && a instanceof RefTypeNode) {
			return a;
		}
		if (a instanceof RefTypeNode && b instanceof RefTypeNode) {
			String left = ((RefTypeNode) a).id;
			while (left != null) {
				final RefTypeNode klass = new RefTypeNode(left);
				if (isSubtype(klass, b)) {
					return b;
				}
				left = superType.get(left);
			}
			return null;
		}
		if (a instanceof ArrowTypeNode && b instanceof ArrowTypeNode) {
			ArrowTypeNode fun1 = (ArrowTypeNode) a;
			ArrowTypeNode fun2 = (ArrowTypeNode) b;
			TypeNode returnAncestor = lowestCommonAncestor(fun1.ret, fun2.ret);
			if (returnAncestor == null || fun1.parlist.size() != fun2.parlist.size()) {
				return null;
			}
			List<TypeNode> parList = new ArrayList<>();
			for (int i = 0; i < fun1.parlist.size(); i++) {
				if (isSubtype(fun1.parlist.get(i), fun2.parlist.get(i))) {
					parList.add(fun1.parlist.get(i));
				} else if (isSubtype(fun2.parlist.get(i), fun1.parlist.get(i))) {
					parList.add(fun2.parlist.get(i));
				} else {
					return null;
				}
			}
			return new ArrowTypeNode(parList, returnAncestor);
		}
		return null;
	}

	private static boolean baseSubtypes(TypeNode a, TypeNode b) {
		return (a.getClass().equals(b.getClass()) && (a instanceof BoolTypeNode || a instanceof IntTypeNode))
				|| (a instanceof BoolTypeNode && b instanceof IntTypeNode);
	}

	private static boolean superType(TypeNode a, TypeNode b) {
		if ((a instanceof EmptyTypeNode && b instanceof EmptyTypeNode)
				|| (a instanceof EmptyTypeNode && b instanceof RefTypeNode)) {
			return true;
		}
		if (a instanceof RefTypeNode && b instanceof RefTypeNode) {
			String subType = ((RefTypeNode) a).id;
			String upType = ((RefTypeNode) b).id;

			while (subType != null) {
				if (subType.equals(upType)) {
					return true;
				}
				subType = superType.get(subType);
			}
		}
		return false;
	}

	private static boolean checkFunctions(TypeNode a, TypeNode b) {
		if (a instanceof ArrowTypeNode && b instanceof ArrowTypeNode) {
			ArrowTypeNode method1 = (ArrowTypeNode) a;
			ArrowTypeNode method2 = (ArrowTypeNode) b;

			return method1.parlist.size() == method2.parlist.size() && isSubtype(method1.ret, method2.ret)
					&& IntStream.range(0, method1.parlist.size())
							.allMatch(n -> isSubtype(method2.parlist.get(n), method1.parlist.get(n)));
		}
		return false;
	}

}
